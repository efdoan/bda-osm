# BDA-OSM

# Description

BDA-OSM focuses on analyzing the decarbonization of Canada’s new and existing residential, commercial, and institutional buildings. The model can customize policy input scenarios and compare the impacts on equipment count, energy consumption, peak load, emission reductions, and cost.

The BDA-OSM is an end-use model and calculates these outputs from energy end uses, allowing for the aggregation of variables on a regional basis. It allows for regional focused analysis, requiring inputs like building counts, fuel rates, population trends, and weather patterns. 

## Authors:

Author: Daniel Bowie

Collaborators: 
Natasha Kettle,
Brent Langille,
Nick Martin,
Mathieu Poirier &
Aurélie Vérin


## Quickstart

1) Clone the repository:
    * `git clone https://gitlab.com/cem-emh/bda-osm.git`

2) Install either [Anaconda](https://www.anaconda.com/) or [Miniconda](https://docs.conda.io/projects/miniconda/en/latest/). During installation make sure you add `conda` to your path variable.

3) Open your local version of copper (the directory, cloned in step 1) and enter the following commands to create and activate the conda environment:
    * `conda env create -f env.yml`
    * `conda activate BDA-OSM` (You have to activate the environment each time the promt is started.)
 
4) Run BDA-OSM
    * `python Run_program_residential.py`

To vizualise the BDA-OSM outputs, the IDEA platform can be used. The steps to install IDEA are described below. For further details, visit the following repository: https://gitlab.com/sesit/idea. 

# IDEA
**Prerequisites**

Before you begin, ensure you have met the following requirements:

- Python (Anaconda recommended)
- Conda package manager

**Installation**

1) Clone the IDEA repository:
    * `git clone https://gitlab.com/sesit-team/idea.git`

2) Change your working directory to the IDEA repository:
    * `cd idea`

3) Create a Conda environment based on the provided environment.yml file:
    * `conda env create -f environment.yml`

4) Activate the Conda environment:
    * `conda activate idea-env`

5) Run the IDEA application using app.py:
    * `python app.py`

IDEA should now be running, and open up a browser window.


